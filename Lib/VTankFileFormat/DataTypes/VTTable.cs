﻿using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace VTankFileFormat.DataTypes {
    class VTTable : VTDataType {
        public string Name = "";
        public int ColumnCount = 0;
        public int RowCount = 0;
        public List<string> ColumnNames = new List<string>();
        public List<bool> ColumnIndexing = new List<bool>();
        public List<Row> Rows = new List<Row>();

        public VTTable(string name = "") {
            this.Name = name;
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteStartElement("table");
            writer.WriteStartElement("columns");
            for (var i = 0; i < ColumnCount; i++) {
                writer.WriteStartElement("column");
                writer.WriteAttributeString("indexed", ColumnIndexing[i].ToString());
                writer.WriteString(ColumnNames[i]);
                writer.WriteEndElement(); // column
            }
            writer.WriteEndElement(); // columns


            writer.WriteStartElement("rows");
            for (var i = 0; i < RowCount; i++) {
                writer.WriteStartElement("row");
                for (var x = 0; x < ColumnCount; x++) {
                    writer.WriteStartElement("col");
                    this[i][x].WriteXML(writer);
                    writer.WriteEndElement(); // row
                }
                writer.WriteEndElement(); // row
            }
            writer.WriteEndElement(); // rows


            writer.WriteEndElement(); // table
        }

        public override void WriteVTF(BinaryWriter writer) {
            WriteVTFString(writer, TypeAsString());
            WriteVTFString(writer, ColumnCount.ToString());

            foreach (var columnName in ColumnNames) {
                WriteVTFString(writer, columnName);
            }
            
            for (var i=0; i < ColumnNames.Count; i++) {
                WriteVTFString(writer, ColumnIndexing[i] ? "y" : "n");
            }

            WriteVTFString(writer, Rows.Count.ToString());

            for (var i = 0; i < Rows.Count; i++) {
                Rows[i].WriteVTF(writer);
            }
        }

        public override string TypeAsString() {
            return string.IsNullOrEmpty(Name) ? "TABLE" : Name;
        }

        public override string ValueAsString() {
            return "[[TABLE]]";
        }

        public Row this[int index] {
            get {
                return Rows[index];
            }

            set {
                Rows[index] = value;
            }
        }

        public void RemoveRowAt(int index) {
            if (index < 0 || index > Rows.Count - 1) return;

            Rows.RemoveAt(index);
            RowCount = Rows.Count;
        }

        public void InsertRowAt(int index, Row row) {
            if (index < 0 || index > Rows.Count - 1) return;

            Rows.Insert(index, row);
            RowCount = Rows.Count;
        }

        internal void AddRow(Row row) {
            Rows.Add(row);
            RowCount = Rows.Count;
        }


        public Row FindRowByColumnValue(string colName, string search) {
            return FindRowByColumnValue(ColumnNames.IndexOf(colName), search);
        }

        public Row FindRowByColumnValue(int col, string search) {
            if (col < 0 || col > ColumnNames.Count - 1) return null;

            foreach (var row in Rows) {
                if (row[col].ValueAsString() == search) return row;
            }

            return null;
        }

        internal override bool Parse(StreamReader sr) {
            if (!ParseColumnCount(sr)) return false;
            if (!ParseColumnNames(sr)) return false;
            if (!ParseColumnIndexes(sr)) return false;
            if (!ParseBody(sr)) return false;

            return true;
        }

        private bool ParseColumnCount(StreamReader sr) {
            try {
                var columnCount = sr.ReadLine();
                Util.WriteDebugLine($"MetTable: parse columnCount: {columnCount}");

                if (!int.TryParse(columnCount, out ColumnCount)) {
                    LastError = $"Unable to parse ColumnCount for table: {columnCount}";
                    return false;
                }

                return true;
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = $"Unable to parse ColumnCount for table";
            return false;
        }

        private bool ParseColumnNames(StreamReader sr) {
            try {
                for (var i = 0; i < ColumnCount; i++) {
                    var columnName = sr.ReadLine();
                    Util.WriteDebugLine($"MetTable: parse col name: {columnName}");

                    if (String.IsNullOrEmpty(columnName)) {
                        LastError = $"Unable to parse ColumnName at column #{i} in table";
                        return false;
                    }

                    ColumnNames.Add(columnName);
                }

                return true;
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = $"Unable to parse ColumnNames for table";
            return false;
        }

        private bool ParseColumnIndexes(StreamReader sr) {
            try {
                for (var i = 0; i < ColumnCount; i++) {
                    bool columnIsIndexed;
                    var line = sr.ReadLine();
                    Util.WriteDebugLine($"MetTable: parse isIndexedCol: {line}");

                    switch (line) {
                        case "y":
                            columnIsIndexed = true;
                            break;

                        case "n":
                            columnIsIndexed = false;
                            break;

                        default:
                            LastError = $"Unable to read column isIndexed value at column #{i} in table";
                            return false;
                    }

                    ColumnIndexing.Add(columnIsIndexed);
                }

                return true;
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = $"Unable to parse isIndexed column values for table";
            return false;
        }

        private bool ParseBody(StreamReader sr) {
            try {
                var rowCount = sr.ReadLine();
                Util.WriteDebugLine($"MetTable: parse row count: {rowCount}");

                if (!int.TryParse(rowCount, out RowCount)) {
                    LastError = $"Unable to parse RowCount for table";
                    return false;
                }
                
                for (var i = 0; i < RowCount; i++) {
                    if (!ParseRow(sr)) {
                        if (string.IsNullOrEmpty(LastError)) LastError = $"Unable to parse row #{i} in table";
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        private bool ParseRow(StreamReader sr) {
            try {
                var row = new Row(this);

                for (var i = 0; i < ColumnCount; i++) {
                    var type = sr.ReadLine();
                    Util.WriteDebugLine($"MetTable: parse row for record type: {type}");

                    VTDataType record = null;

                    switch (type) {
                        case "TABLE": // table obv
                            record = new VTTable();
                            break;

                        case "i": // integer
                            record = new VTInteger();
                            break;

                        case "d": // double
                            record = new VTDouble();
                            break;

                        case "ba": // byte array
                            record = new VTByteArray();
                            break;

                        case "s": // string
                            record = new VTString();
                            break;

                        case "b": // boolean
                            record = new VTBoolean();
                            break;
                    }

                    if (record == null) {
                        LastError = "Something went wrong parsing child table row, unknown type maybe: " + type + "::" + sr.ReadLine();
                        return false;
                    }

                    if (!record.Parse(sr)) {
                        LastError = record.LastError;
                        if (string.IsNullOrEmpty(LastError)) LastError = "Something went wrong parsing child table row.";
                        return false;
                    }

                    row[ColumnNames[i]] = record;
                }

                Rows.Add(row);

                return true;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void Print() {
            Console.WriteLine($"Table:");
            Console.WriteLine($"\tColumn Count: {ColumnCount}");
            Console.WriteLine($"\tColumn Names: {String.Join(", ", ColumnNames.ToArray())}");
            Console.WriteLine($"\tColumn IsIndexed: {String.Join(", ", ColumnIndexing.Select(i => i ? "yes" : "no").ToArray())}");
            Console.WriteLine(string.Join("\t", ColumnNames.ToArray()));

            for (var i = 0; i < RowCount; i++) {
                this[i].Print();
            }
        }

        public string GetValues() {
            var values = new List<string>();

            for (var i=0; i < RowCount; i++) {
                values.Add(this[i][1].ValueAsString());
            }

            return String.Join(";", values.ToArray());
        }

        public class Row {
            public VTTable ParentTable;
            
            Dictionary<string, VTDataType> Data = new Dictionary<string, VTDataType>();

            public Row(VTTable parentTable) {
                ParentTable = parentTable;
            }

            public VTDataType this[int index] {
                get {
                    var name = index < ParentTable.ColumnNames.Count ? ParentTable.ColumnNames[index] : null;
                    return string.IsNullOrEmpty(name) ? null : this[name];
                }

                set {
                    var name = index < ParentTable.ColumnNames.Count ? ParentTable.ColumnNames[index] : null;
                    this[name] = value;
                }
            }

            public VTDataType this[string columnName] {
                get {
                    return Data[columnName];
                }

                set {
                    Data[columnName] = value;
                }
            }

            public void Print() {
                foreach (var d in Data) {
                    Console.Write($"{d.Value.ValueAsString()}\t");
                }

                Console.Write("\n");
            }

            internal void WriteVTF(BinaryWriter writer) {
                for (var i = 0; i < ParentTable.ColumnNames.Count; i++) {
                    this[i].WriteVTF(writer);
                }
            }
        }
    }
}
