﻿using System;
using System.IO;
using System.Xml;

namespace VTankFileFormat.DataTypes {
    internal abstract class VTDataType {
        public string LastError;

        internal abstract bool Parse(StreamReader sr);
        public abstract void Print();

        public abstract string TypeAsString();
        public abstract string ValueAsString();
        public abstract void WriteXML(XmlWriter writer);

        public virtual bool SetValue(string newValue) {
            throw new NotImplementedException();
        }

        public static void WriteVTFString(BinaryWriter writer, string data) {
            foreach (char c in data) {
                writer.Write(c);
            }
            writer.Write('\n');
        }

        public virtual void WriteVTF(BinaryWriter writer) {
            WriteVTFString(writer, TypeAsString());
            WriteVTFString(writer, ValueAsString());
        }
    }
}