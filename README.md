## MetaHelper

Total WIP, will probably change names and stuff later.

## Commands

* `/mh vt addequippable <item name or id>`
  - Adds `<item name or id>` to vtank's equippable item tab
* `/mh vt addconsumable <item name> <type>`
  - Adds `<item name>` to vtank's consumables tab.  I don't really know what type is yet, here is a list of known values:
    * 0: Healing Kit
    * 1: ??
	* 2: ??
	* 3: Stamina Food?
	* 6: Mana Charge
	* 8: Mana Stone
	* 10: Lockpick
* `/mh swear to <character name>`
	- Attemps to swear allegiance to `<character name>`