﻿using MetaHelper.Lib;
using System;
using System.IO;
using System.Xml;

namespace VTankFileFormat.DataTypes {
    internal class VTBoolean : VTDataType {
        public bool Value = false;

        public VTBoolean() {

        }

        public VTBoolean(bool value) {
            Value = value;
        }

        public override void Print() {
            Console.Write($"{Value.ToString()}");
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("boolean", Value.ToString());
        }

        public override string TypeAsString() {
            return "b";
        }

        public override string ValueAsString() {
            return Value.ToString();
        }

        public override bool SetValue(string newValue) {
            newValue = newValue.ToLower();

            if (newValue == "true" || newValue == "yes" || newValue == "y") {
                Value = true;
            }
            else if (newValue == "false" || newValue == "no" || newValue == "n") {
                Value = false;
            }
            else {
                LastError = $"Unable to parse boolean from: {newValue}";
                return false;
            }

            return true;
        }

        internal override bool Parse(StreamReader sr) {
            try {
                var line = sr.ReadLine();
                Util.WriteDebugLine($"MetBoolean: parse value: {line}");

                if (line == "True") {
                    Value = true;
                }
                else if (line == "False") {
                    Value = false;
                }
                else {
                    LastError = "MetBoolean: Unable to parse: " + line;
                    return false;
                }

                return true;
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = "Unable to parse integer.";
            return false;
        }
    }
}