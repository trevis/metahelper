﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VTankFileFormat.Constants {
    enum ActionType {
        None = 0,
        SetState,
        ChatCommand,
        Multiple,
        EmbeddedNavRoute,
        CallState,
        ReturnFromCall,
        ExpressionAct,
        ChatWithExpression,
        WatchdogSet,
        WatchdogClear,
        GetVTOption,
        SetVTOption,
        CreateView,
        DestroyView,
        DestroyAllViews
    }
}
