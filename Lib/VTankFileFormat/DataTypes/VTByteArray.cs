﻿using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace VTankFileFormat.DataTypes {
    internal class VTByteArray : VTDataType {
        public string Data = "";
        public string DataFile = @"C:\doesnt-exist.nav";

        private static int navFileIndex = 1;

        public VTByteArray() {

        }

        public VTByteArray(string data) {
            Data = data;
        }

        public override void Print() {
            Console.Write($"{Data}");
        }

        public override string TypeAsString() {
            return "ba";
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("bytearray", Data);
        }

        new public void WriteVTF(BinaryWriter writer) {
            WriteVTFString(writer, TypeAsString());
            WriteVTFString(writer, (Data.Length * sizeof(Char)).ToString());
            WriteVTFString(writer, Data);
        }

        public override string ValueAsString() {
            return DataFile.ToString();
        }

        public override bool SetValue(string newValue) {
            Data = newValue;

            return true;
        }

        internal override bool Parse(StreamReader sr) {
            try {
                int bytesToRead;
                var byteLine = sr.ReadLine();
                Util.WriteDebugLine($"MetByteArray: parse route bytes: {byteLine}");

                if (!int.TryParse(byteLine, out bytesToRead)) {
                    LastError = "Error parsing byteLine for MetByteArray";
                    return false;
                }

                var buffer = new char[bytesToRead];
                sr.Read(buffer, 0, buffer.Length);

                Data = bytesToRead > 0 ? new string(buffer) : "[None]";

                // dirty
                var navName = Data.Split('\n')[0].Trim();
                if (navName.ToLower() != "[none]" && !navName.ToLower().EndsWith(".nav")) {
                    DataFile = Data;
                    return true;
                }
                else {
                    if (navName == "[None]") navName = "None";
                    var outputPath = $"{navName}-{navFileIndex++}.nav";
                    var parts = Data.Split('\n');
                    var p = new List<string>(parts);
                    p.RemoveAt(0);
                    p.RemoveAt(0);
                    Data = string.Join("\n", p.ToArray());

                    File.WriteAllText(outputPath, Data);
                    DataFile = outputPath;
                    return true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = "Unable to parse string.";
            return false;
        }
    }
}