﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VTankFileFormat.DataTypes;
using VTankFileFormat.Parsers;

namespace MetaHelper.Tools {
    class VTank : IDisposable {
        public static string VTankDirectory = @"C:\Games\VirindiPlugins\VirindiTank\";

        bool disposed = false;

        public VTank(PluginCore pluginCore) {
            CoreManager.Current.CommandLineText += Current_CommandLineText;
        }

        private void Current_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (e.Text.StartsWith("/mh vt addequippable ")) {
                    e.Eat = true;
                    var item = e.Text.Replace("/mh vt addequippable ", "");
                    var id = 0;

                    if (!Int32.TryParse(item, out id)) {
                        foreach (var wo in CoreManager.Current.WorldFilter.GetInventory()) {
                            if (wo.Name == item) AddEquippableItem(wo.Id);
                        }
                    }
                    else {
                        AddEquippableItem(id);
                    }
                }
                else if (e.Text.StartsWith("/mh vt addconsumable ")) {
                    e.Eat = true;
                    var parts = e.Text.Replace("/mh vt addconsumable ", "").Split(' ');

                    if (parts.Length < 2) {
                        Util.WriteToChat("Usage: /mh vt addconsumable Healing Kit <type>");
                        return;
                    }

                    var item = string.Join(" ", parts.Take(parts.Length - 1).ToArray());
                    var typePart = parts[parts.Length - 1];
                    int type = 0;

                    if (!Int32.TryParse(typePart, out type)) {
                        Util.WriteToChat("Could not parse type to int: " + typePart);
                        return;
                    }

                    AddConsumableItem(item, type);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private string GetCharacterProfileName() {
            var character = Util.CleanCharacterName(CoreManager.Current.CharacterFilter.Name);
            var server = CoreManager.Current.CharacterFilter.Server;
            return $"{server}_{character}";
        }

        private string GetCharacteProfilesFilesPath() {
            return Path.Combine(VTankDirectory, $"{GetCharacterProfileName()}.cdf");
        }

        private void AddEquippableItem(int id) {
            try {
                var profilesParser = GetProfilesParser();
                var characterSettingsPath = Path.Combine(VTankDirectory, profilesParser.CharacterSettings);
                var settingsParser = new VTFSettingsParser(characterSettingsPath);

                if (settingsParser.Parse()) {
                    var BuffedItems = settingsParser.FindTableByName("BuffedItems");

                    if (BuffedItems != null) {
                        var row = new VTTable.Row(BuffedItems);
                        row[0] = new VTInteger(id); // item id
                        row[1] = new VTInteger(0);  // spell id (we just do 0 and let vtank fill it in later, usually its one entry per spell)
                        BuffedItems.AddRow(row);
                    }

                    settingsParser.ToUSD(characterSettingsPath);

                    var wo = CoreManager.Current.WorldFilter[id];
                    var name = wo == null ? $"<{id}>" : wo.Name;
                    var settingsFile = characterSettingsPath.Split('\\').Last().Replace(".usd", "");

                    Util.WriteToChat($"Added {name} to VTank equippable items list.  Reloading Character Settings: {settingsFile}");
                    //Util.WriteToChat($"/vt settings load {settingsFile}");
                    Util.DispatchChatToBoxWithPluginIntercept($"/vt settings load {settingsFile}");
                }
                else {
                    Util.WriteToChat("Could not parse settings:");
                    Util.WriteToChat(settingsParser.LastError);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void AddConsumableItem(string name, int type) {
            try {
                var profilesParser = GetProfilesParser();
                var characterSettingsPath = Path.Combine(VTankDirectory, profilesParser.CharacterSettings);
                var settingsParser = new VTFSettingsParser(characterSettingsPath);

                if (settingsParser.Parse()) {
                    var AssistItems = settingsParser.FindTableByName("AssistItems");

                    if (AssistItems != null) {
                        foreach (var existingRow in AssistItems.Rows) {
                            if (existingRow[0].ValueAsString() == name) {
                                Util.WriteToChat($"{name} is already in the VTank consumables list");
                                return;
                            }
                        }

                        var newRow = new VTTable.Row(AssistItems);
                        newRow[0] = new VTString(name); // item name
                        /*
                         * So I think these are the types:
                         * 0: Healing Kit
                         * 1: ??
                         * 2: ??
                         * 3: Stamina Food?
                         * 6: Mana Charge
                         * 8: Mana Stone
                         * 10: Lockpick
                         */
                        newRow[1] = new VTInteger(type); // type ?
                        AssistItems.AddRow(newRow);
                    }

                    settingsParser.ToUSD(characterSettingsPath);
                    
                    var settingsFile = characterSettingsPath.Split('\\').Last().Replace(".usd", "");

                    Util.WriteToChat($"Added {name} to VTank consumable items list.  Reloading Character Settings: {settingsFile}");
                    Util.DispatchChatToBoxWithPluginIntercept($"/vt settings load {settingsFile}");
                }
                else {
                    Util.WriteToChat("Could not parse settings:");
                    Util.WriteToChat(settingsParser.LastError);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private VTFCharacterProfilesParser GetProfilesParser() {
            var profilesPath = GetCharacteProfilesFilesPath();
            var profilesParser = new VTFCharacterProfilesParser(profilesPath);

            if (!profilesParser.Parse()) {
                Util.WriteToChat("Could not parse InFile: " + profilesPath);
                Util.WriteToChat(profilesParser.LastError);
                return null;
            }

            if (string.IsNullOrEmpty(profilesParser.CharacterSettings)) {
                Util.WriteToChat("Character settings file is null inside of cdf, no support for creating new settings files (yet!).");
                return null;
            }

            return profilesParser;
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    CoreManager.Current.CommandLineText -= Current_CommandLineText;
                }
                disposed = true;
            }
        }
    }
}
