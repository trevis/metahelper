﻿using System;
using Decal.Adapter;
using MetaHelper.Lib;
using System.IO;
using MetaHelper.Tools;

namespace MetaHelper {

    [WireUpBaseEvents]
    [FriendlyName("MetaHelper")]

    public class PluginCore : PluginBase {
        internal Allegiance allegiance;
        internal VTank vtank;
        internal Client client;

        protected override void Startup() {
            try {
                Util.Host = Host;
                Util.CreatePluginDirectories();

                CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                allegiance = new Allegiance(this);
                vtank = new VTank(this);
                client = new Client(this);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected override void Shutdown() {
            try {
                CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
                if (allegiance != null) allegiance.Dispose();
                if (vtank != null) vtank.Dispose();
                if (client != null) client.Dispose();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
 
 