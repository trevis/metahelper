﻿using MetaHelper.Lib;
using System;
using System.IO;
using System.Xml;

namespace VTankFileFormat.DataTypes {
    internal class VTInteger : VTDataType {
        public int Value = -1;

        public VTInteger() {

        }

        public VTInteger(int value) {
            Value = value;
        }

        public override void Print() {
            Console.Write($"{Value}");
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("integer", Value.ToString());
        }

        public override string TypeAsString() {
            return "i";
        }

        public override string ValueAsString() {
            return Value.ToString();
        }

        public override bool SetValue(string newValue) {
            if (!int.TryParse(newValue, out Value)) {
                LastError = "Unable to parse integer from: " + newValue;
                return false;
            }

            return true;
        }

        internal override bool Parse(StreamReader sr) {
            try {
                var line = sr.ReadLine();
                Util.WriteDebugLine($"MetInteger: parse value: {line}");

                return SetValue(line);
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = "Unable to parse integer.";
            return false;
        }
    }
}