﻿using MetaHelper.Lib;
using System;
using System.IO;
using System.Xml;

namespace VTankFileFormat.DataTypes {
    internal class VTDouble : VTDataType {
        public double Value = -1;

        public VTDouble() {

        }

        public VTDouble(double value) {
            Value = value;
        }

        public override void Print() {
            Console.Write($"{Value}");
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("double", Value.ToString());
        }

        public override string TypeAsString() {
            return "d";
        }

        public override string ValueAsString() {
            return Value.ToString();
        }

        public override bool SetValue(string newValue) {
            if (!double.TryParse(newValue, out Value)) {
                LastError = "Unable to parse double from: " + newValue;
                return false;
            }

            return true;
        }

        internal override bool Parse(StreamReader sr) {
            try {
                var line = sr.ReadLine();
                Util.WriteDebugLine($"MetDouble: parse value: {line}");

                return SetValue(line);
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = "Unable to parse integer.";
            return false;
        }
    }
}