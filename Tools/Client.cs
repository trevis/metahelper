﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;

namespace MetaHelper.Tools {
    class Client : IDisposable {
        const int PORTALSPACE_TIMEOUT_SECONDS = 60;


        bool disposed = false;
        DateTime lastPortalEntered = DateTime.UtcNow;
        Timer portalSpaceTimer;
        PluginCore pluginCore;

        public Client(PluginCore pluginCore) {
            this.pluginCore = pluginCore;
            CoreManager.Current.CharacterFilter.ChangePortalMode += CharacterFilter_ChangePortalMode;
            CoreManager.Current.CommandLineText += Current_CommandLineText;
        }

        private void Current_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (e.Text.StartsWith("/mh client kill")) {
                    e.Eat = true;
                    Kill();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void PortalSpaceTimer_Elapsed(object sender, ElapsedEventArgs e) {
            try {
                Kill();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CharacterFilter_ChangePortalMode(object sender, ChangePortalModeEventArgs e) {
            try {
                switch (e.Type) {
                    case PortalEventType.EnterPortal:
                        if (portalSpaceTimer != null) {
                            portalSpaceTimer.Stop();
                            portalSpaceTimer = null;
                        }

                        portalSpaceTimer = new Timer(PORTALSPACE_TIMEOUT_SECONDS * 1000);
                        portalSpaceTimer.Elapsed += PortalSpaceTimer_Elapsed;
                        portalSpaceTimer.Start();
                        Util.WriteToChat("Entered portal space, starting stuck timer.");
                        lastPortalEntered = DateTime.UtcNow;
                        break;

                    case PortalEventType.ExitPortal:
                        if (portalSpaceTimer != null && portalSpaceTimer.Enabled) {
                            Util.WriteToChat($"Exiting portal space, stuck timer cancelled.  Elapsed: {DateTime.UtcNow - lastPortalEntered}");
                            portalSpaceTimer.Stop();
                        }
                        break;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void Kill() {
            Util.WriteToChat("Killing client");
            InputManager.KillProcess();
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    CoreManager.Current.CommandLineText -= Current_CommandLineText;
                    CoreManager.Current.CharacterFilter.ChangePortalMode -= CharacterFilter_ChangePortalMode;
                }
                disposed = true;
            }
        }
    }
}
