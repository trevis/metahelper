﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MetaHelper.Tools {
    class Allegiance : IDisposable {
        bool disposed = false;
        int currentActionIndex = 0;
        int swearToId = 0;
        DateTime lastAction = DateTime.UtcNow;

        const int ACTION_DELAY_MS = 100;

        public Allegiance(PluginCore pluginCore) {
            CoreManager.Current.CommandLineText += Current_CommandLineText;
        }

        public void SwearTo(int id) {
            try {
                var woPlayer = CoreManager.Current.WorldFilter[id];

                if (woPlayer == null) {
                    Util.WriteToChat($"Unable to find player with id {id}");
                    return;
                }

                Util.WriteToChat($"Attempting to swear to {woPlayer.Name}");

                currentActionIndex = 0;
                swearToId = id;

                CoreManager.Current.RenderFrame += Current_RenderFrame;

            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void SwearTo(string characterName) {
            try {
                var allPlayers = CoreManager.Current.WorldFilter.GetByObjectClass(ObjectClass.Player);

                var characterNameLowered = characterName.ToLower();

                foreach (var player in allPlayers) {
                    if (player != null && player.Name.ToLower() == characterNameLowered) {
                        SwearTo(player.Id);
                        return;
                    }
                }

                Util.WriteToChat($"Unable to find player: {characterName}");
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                if (DateTime.UtcNow - lastAction < TimeSpan.FromMilliseconds(ACTION_DELAY_MS)) return;

                lastAction = DateTime.UtcNow;

                switch (currentActionIndex) {
                    case 0:
                        CoreManager.Current.Actions.SelectItem(swearToId);
                        break;
                    case 1:
                        Util.WriteToChat("F12");
                        InputManager.SendKey((IntPtr)InputManager.VK_F12, false);
                        break;
                    case 2:
                        Util.WriteToChat("F3");
                        InputManager.SendKey((IntPtr)InputManager.VK_F3, false);
                        break;
                    case 3:
                        Util.WriteToChat("Click Swear");
                        Rectangle rect = CoreManager.Current.Actions.UIElementRegion(UIElementType.Panels);
                        // click swear button
                        InputManager.LeftClick(rect.X + 50, rect.Y + rect.Height - 30);
                        break;
                    case 4:
                        Util.WriteToChat("Click Yes");
                        InputManager.ClickYes();
                        break;

                    default:
                        Util.WriteToChat("Done");
                        CoreManager.Current.RenderFrame -= Current_RenderFrame;
                        break;
                }

                currentActionIndex++;

            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Current_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (e.Text.StartsWith("/mh swear to ")) {
                    e.Eat = true;
                    SwearTo(e.Text.Replace("/mh swear to ", "").Trim());
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    CoreManager.Current.CommandLineText -= Current_CommandLineText;
                }
                disposed = true;
            }
        }
    }
}
