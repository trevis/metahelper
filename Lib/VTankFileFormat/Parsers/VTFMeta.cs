﻿using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using VTankFileFormat.DataTypes;

namespace VTankFileFormat.Parsers {
    class VTFMetaParser {
        public int VTFileFormat;
        public string LastError;
        public string FileName;
        
        public VTTable MainTable;

        public VTFMetaParser(string fileName) {
            FileName = fileName;
        }

        public bool Parse() {
            try {
                if (!File.Exists(FileName)) {
                    LastError = "Could not find meta file: " + FileName;
                    return false;
                }

                using (StreamReader sr = File.OpenText(FileName)) {
                    string line = sr.ReadLine();
                    Util.WriteDebugLine($"Parser header: {line}");

                    if (!int.TryParse(line, out VTFileFormat)) {
                        SetError("Could not read header: " + line);
                        return false;
                    }

                    string mainTable = sr.ReadLine();
                    Util.WriteDebugLine($"Parser main table: {mainTable}");

                    MainTable = new VTTable(mainTable);

                    if (!MainTable.Parse(sr)) {
                        LastError = MainTable.LastError;
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            SetError("Could not parse meta file: " + FileName);
            return false;
        }

        internal void ToXML(string outputFile) {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            XmlWriter writer = XmlWriter.Create(outputFile, settings);

            MainTable.WriteXML(writer);

            writer.Flush();
            writer.Close();
        }

        internal void ToVTF(string outputFile) {
            FileStream fs = new FileStream(outputFile, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(fs);

            // version
            writer.Write(VTFileFormat + "\n");

            MainTable.WriteVTF(writer);

            writer.Flush();
            writer.Close();

            fs.Close();
        }

        public void Print() {
            Util.WriteLine($"Meta File: {FileName.Split(Path.DirectorySeparatorChar).Last()}");
            Util.WriteLine($"Meta Version: {VTFileFormat}");

            MainTable.Print();
        }

        private void SetError(string error) {
            LastError = error;
        }
    }
}
