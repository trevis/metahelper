﻿using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using VTankFileFormat.DataTypes;

namespace VTankFileFormat.Parsers {
    class VTFSettingsParser {
        public int FileFormat;
        public string LastError;
        public string FileName;

        public List<VTTable> Data = new List<VTTable>();

        public VTFSettingsParser(string fileName) {
            FileName = fileName;
        }

        public VTTable FindTableByName(string search) {
            foreach (var table in Data) {
                if (table.Name == search) return table;
            }

            return null;
        }

        public bool Parse() {
            try {
                if (!File.Exists(FileName)) {
                    LastError = "Could not find settings file: " + FileName;
                    return false;
                }

                using (StreamReader sr = File.OpenText(FileName)) {
                    string line = sr.ReadLine();
                    Util.WriteDebugLine($"Parser header: {line}");

                    if (!int.TryParse(line, out FileFormat)) {
                        SetError("Could not read header: " + line);
                        return false;
                    }

                    while (true) {
                        if (sr.EndOfStream) {
                            Util.WriteDebugLine("EOS");
                            break;
                        }
                        
                        var name = sr.ReadLine();
                        VTTable record = new VTTable(name);

                        if (!record.Parse(sr)) {
                            LastError = record.LastError;
                            return false;
                        }

                        Data.Add(record);
                    }

                    return true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            SetError("Could not parse meta file: " + FileName);
            return false;
        }

        internal void ToXML(string outputFile) {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            XmlWriter writer = XmlWriter.Create(outputFile, settings);

            foreach (var record in Data) {
                record.WriteXML(writer);
            }

            writer.Flush();
            writer.Close();
        }

        internal void ToUSD(string outputFile) {
            FileStream fs = new FileStream(outputFile, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(fs);

            VTDataType.WriteVTFString(writer, FileFormat.ToString());

            foreach (var record in Data) {
                record.WriteVTF(writer);
            }

            writer.Flush();
            writer.Close();

            fs.Close();
        }

        public void Print() {
            Util.WriteLine($"Settings File: {FileName.Split(Path.DirectorySeparatorChar).Last()}");
            Util.WriteLine($"Settings Format: {FileFormat}");

            foreach (var record in Data) {
                record.Print();
            }
        }

        private void SetError(string error) {
            LastError = error;
        }
    }
}
