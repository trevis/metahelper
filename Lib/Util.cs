﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Decal.Adapter;
using System.Runtime.InteropServices;

namespace MetaHelper.Lib {
    internal static class Util {
        public static PluginHost Host { get; internal set; }

        internal static void CreatePluginDirectories() {
            //Directory.CreateDirectory(GetPluginStoragePath());
        }

        internal static string GetPluginStoragePath() {
            string PluginPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Decal Plugins\";
            return PluginPath;
        }

        internal static void LogException(Exception ex) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(GetPluginStoragePath(), "MetaHelper-exceptions.txt"), true)) {
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Error: " + ex.Message);
                    writer.WriteLine("Source: " + ex.Source);
                    writer.WriteLine("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        writer.WriteLine("Inner: " + ex.InnerException.Message);
                        writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();

                    Util.WriteToChat("Error: " + ex.Message);
                    Util.WriteToChat("Source: " + ex.Source);
                    Util.WriteToChat("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        Util.WriteToChat("Inner: " + ex.InnerException.Message);
                        Util.WriteToChat("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                }
            }
            catch {
            }
        }

        internal static void WriteDebugLine(string v) {

        }

        internal static object CleanCharacterName(string name) {
            return name.Replace("+", "");
        }

        internal static Assembly GetAssembly() {
            return Assembly.GetExecutingAssembly();
        }

        internal static void WriteToChat(string message) {
            try {
                CoreManager.Current.Actions.AddChatText("[MetaHelper] " + message, 1);
            }
            catch (Exception ex) { LogException(ex); }
        }


        [DllImport("Decal.dll")]
        static extern int DispatchOnChatCommand(ref IntPtr str, [MarshalAs(UnmanagedType.U4)] int target);

        public static bool Decal_DispatchOnChatCommand(string cmd) {
            IntPtr bstr = Marshal.StringToBSTR(cmd);

            try {
                bool eaten = (DispatchOnChatCommand(ref bstr, 1) & 0x1) > 0;

                return eaten;
            }
            finally {
                Marshal.FreeBSTR(bstr);
            }
        }

        public static void DispatchChatToBoxWithPluginIntercept(string cmd) {
            if (!Decal_DispatchOnChatCommand(cmd))
                CoreManager.Current.Actions.InvokeChatParser(cmd);
        }

        internal static void WriteLine(string message) {
            CoreManager.Current.Actions.AddChatText("[MH] " + message, 3);
        }

        internal static void Think(string message) {
            try {
                DispatchChatToBoxWithPluginIntercept(string.Format("/tell {0}, {1}", CoreManager.Current.CharacterFilter.Name, message));
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}