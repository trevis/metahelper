﻿using MetaHelper.Lib;
using System;
using System.IO;
using System.Xml;

namespace VTankFileFormat.DataTypes {
    internal class VTString : VTDataType {
        public string Value = "";

        public VTString() {

        }

        public VTString(string value) {
            Value = value;
        }

        public override void Print() {
            Console.Write($"{Value}");
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("string", Value);
        }

        public override string TypeAsString() {
            return "s";
        }

        public override string ValueAsString() {
            return Value;
        }

        public override bool SetValue(string newValue) {
            Value = newValue;
            return true;
        }

        internal override bool Parse(StreamReader sr) {
            try {
                var line = sr.ReadLine();
                Util.WriteDebugLine($"VTString: parse value: {line}");

                return SetValue(line);
            }
            catch (Exception ex) { Util.LogException(ex); }

            LastError = "Unable to parse string.";
            return false;
        }
    }
}