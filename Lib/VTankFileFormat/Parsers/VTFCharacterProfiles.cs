﻿

using MetaHelper.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using VTankFileFormat.DataTypes;

/*
uTank2 CDF 1.0                  // header
--Sunnuj_Coldeve.usd            // character settings
facilityHubQuests.utl           // loot profile
!caul.nav                       // nav
NewCharacter-PathWarden.met     // meta

 */

namespace VTankFileFormat.Parsers {
    class VTFCharacterProfilesParser {
        public string FileName = "";

        public string FileFormat = "uTank2 CDF 1.0";
        public string CharacterSettings = "";
        public string LootProfile = "";
        public string NavProfile = "";
        public string MetaProfile = "";

        public string LastError = "";

        public VTFCharacterProfilesParser(string fileName) {
            FileName = fileName;
        }

        public bool Parse() {
            try {
                if (!File.Exists(FileName)) {
                    LastError = "Could not find CharacterProfiles file: " + FileName;
                    return false;
                }

                using (StreamReader sr = File.OpenText(FileName)) {
                    string formatLine = sr.ReadLine();
                    Util.WriteDebugLine($"Parser header: {formatLine}");

                    FileFormat = formatLine;

                    CharacterSettings = sr.ReadLine();
                    LootProfile = sr.ReadLine();
                    NavProfile = sr.ReadLine();
                    MetaProfile = sr.ReadLine();

                    return true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            SetError("Could not parse CharacterProfiles file: " + FileName);
            return false;
        }

        internal void ToXML(string outputFile) {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            XmlWriter writer = XmlWriter.Create(outputFile, settings);

            writer.WriteElementString("CharacterSettings", CharacterSettings);
            writer.WriteElementString("LootProfile", LootProfile);
            writer.WriteElementString("NavProfile", NavProfile);
            writer.WriteElementString("MetaProfile", MetaProfile);

            writer.Flush();
            writer.Close();
        }

        internal void ToCDF(string outputFile) {
            FileStream fs = new FileStream(outputFile, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(fs);

            VTDataType.WriteVTFString(writer, FileFormat);

            VTDataType.WriteVTFString(writer, CharacterSettings);
            VTDataType.WriteVTFString(writer, LootProfile);
            VTDataType.WriteVTFString(writer, NavProfile);
            VTDataType.WriteVTFString(writer, MetaProfile);

            writer.Flush();
            writer.Close();

            fs.Close();
        }

        public void Print() {
            Util.WriteLine($"Settings File: {FileName.Split(Path.DirectorySeparatorChar).Last()}");
            Util.WriteLine($"Settings Format: {FileFormat}");

            Util.WriteLine($"CharacterSettings: {CharacterSettings}");
            Util.WriteLine($"LootProfile: {LootProfile}");
            Util.WriteLine($"NavProfile: {NavProfile}");
            Util.WriteLine($"MetaProfile: {MetaProfile}");
        }

        private void SetError(string error) {
            LastError = error;
        }
    }
}
