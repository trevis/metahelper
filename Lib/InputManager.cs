﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace MetaHelper.Lib {
    public static class InputManager {
        public const int WM_DESTROY = 0x0002;
        public const int WM_KILLFOCUS = 0x0008;
        public const int WM_KEYDOWN = 0x0100;
        public const int WM_KEYUP = 0x0101;
        public const int WM_CHAR = 0x0102;

        public const int WM_MOUSEMOVE = 0x0200;
        public const int WM_LBUTTONDOWN = 0x0201;
        public const int WM_LBUTTONUP = 0x0202;

        public const byte VK_HOME = 0x24;
        public const byte VK_DELETE = 0x2E;
        public const byte VK_SHIFT = 0x10;
        public const byte VK_LSHIFT = 0xA0;
        public const byte VK_CAPITAL = 0x14;
        public const byte VK_BACK = 0x08;

        public const byte VK_F3 = 0x72;
        public const byte VK_F4 = 0x73;
        public const byte VK_F12 = 0x7B;

        private const uint KEYEVENTF_UNICODE = 0x0004;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern bool PostMessage(IntPtr hhwnd, uint msg, IntPtr wparam, UIntPtr lparam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImportAttribute("User32.dll")]
        internal static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern uint MapVirtualKey(uint uCode, uint uMapType);

        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int uMsg, int wParam, string lParam);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

            public int Width { get { return Right - Left; } }
            public int Height { get { return Bottom - Top; } }
        }

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        public static void SendKey(IntPtr keyCode, bool extended) {
            uint scanCode = MapVirtualKey((uint)keyCode, 0);
            uint lParam;

            lParam = (0x00000001 | (scanCode << 16));
            if (extended) {
                lParam |= 0x01000000;
            }

            PostMessage(CoreManager.Current.Decal.Hwnd, WM_KEYDOWN, keyCode, (UIntPtr)lParam);

            lParam |= 0xC0000000;
            PostMessage(CoreManager.Current.Decal.Hwnd, WM_KEYUP, keyCode, (UIntPtr)lParam);
        }

        public static void SendKeys(string keys) {
            foreach (char key in keys) {
                PostMessage(CoreManager.Current.Decal.Hwnd, WM_CHAR, (IntPtr)key, (UIntPtr)0);
            }
        }

        public static void LeftClick(int x, int y) {
            int loc = (y * 0x10000) + x;

            PostMessage(CoreManager.Current.Decal.Hwnd, WM_MOUSEMOVE, (IntPtr)0x00000000, (UIntPtr)loc);
            PostMessage(CoreManager.Current.Decal.Hwnd, WM_LBUTTONDOWN, (IntPtr)0x00000001, (UIntPtr)loc);
            PostMessage(CoreManager.Current.Decal.Hwnd, WM_LBUTTONUP, (IntPtr)0x00000000, (UIntPtr)loc);
        }

        public static void ClickYes() {
            RECT rect = new RECT();

            GetWindowRect(CoreManager.Current.Decal.Hwnd, ref rect);

            // 800x600 +32 works, +33 does not work on single/double/tripple line boxes
            // 1600x1200 +31 works, +32 does not work on single/double/tripple line boxes
            // The reason why we click at both of these positions is some clients will be running windowed, and some windowless. This will hit both locations
            LeftClick(rect.Width / 2 - 80, rect.Height / 2 + 18);
            LeftClick(rect.Width / 2 - 80, rect.Height / 2 + 25);
            LeftClick(rect.Width / 2 - 80, rect.Height / 2 + 31);
        }

        public static void KillProcess() {
            PostMessage(CoreManager.Current.Decal.Hwnd, WM_DESTROY, (IntPtr)0, (UIntPtr)0);
        }
    }
}
